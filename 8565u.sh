#!/bin/sh
#===============================================================================
# Powerlimit adjust script for i7-8565u
#
# Powerlimit adjustment
# 0x42814000dd8140
# cpu0: PKG Limit #1: ENabled (40.000000 Watts, 28.000000 sec, clamp ENabled)
# cpu0: PKG Limit #2: ENabled (40.000000 Watts, 0.002441* sec, clamp DISabled)
#
# Turbo adjustment
# 0x2e2e2e2e
# 46 * 100.0 = 4600.0 MHz max turbo 4 active cores
# 46 * 100.0 = 4600.0 MHz max turbo 3 active cores
# 46 * 100.0 = 4600.0 MHz max turbo 2 active cores
#46 * 100.0 = 4600.0 MHz max turbo 1 active cores
#===============================================================================

echo "Setting MSR_CONFIG_TDP_CONTROL [0x64b] to [0x2]"
echo "Old value = [0x`rdmsr -x 0x64b`]"
wrmsr 0x64b 0x2
echo "New value = [0x`rdmsr -x 0x64b`]"

#===============================================================================

echo "Setting MSR_PACKAGE_POWER_LIMIT [0x610] to [0x42816000dd8160]"
echo "Old value = [0x`rdmsr -x 0x610`]"
wrmsr 0x610 0x42814000dd8140
echo "New value = [0x`rdmsr -x 0x610`]"

#===============================================================================
DEVMEM=/dev/mem
if [ -f "$DEVMEM" ]; then
	echo "OS supports direct access to $DEVMEM"
	echo "Setting MCHBAR PACKAGE_POWER_LIMIT [DIRECT MEMORY 0xfed159a0] to [0x42816000dd8160]"
	echo "Old value"
	devmem2 0xfed159a4 word | grep -i value
	devmem2 0xfed159a0 word | grep -i value

	devmem2 0xfed159a0 word 0x00dd8140
	devmem2 0xfed159a4 word 0x00428140

	echo "New value"
	devmem2 0xfed159a4 word | grep -i value
	devmem2 0xfed159a0 word | grep -i value
else
    echo "Direct access to $DEVMEM does not exist"
    echo "Unable to set MCHBAR PACKAGE_POWER_LIMIT"
fi

#===============================================================================

echo "Setting MSR_TURBO_RATIO_LIMIT [0x1ad] to [0x2e2e2e2e]"
echo "Old value = [0x`rdmsr -x 0x1ad`]"
wrmsr 0x1ad 0x2e2e2e2e
echo "New value = [0x`rdmsr -x 0x1ad`]"

#===============================================================================
