#!/bin/bash

# Define the CPU specific script that will be used
CPU_SH=8565u.sh

# Copy main script
sudo cp -v ${CPU_SH} /usr/bin/intel-pwr-limit-adj
sudo chmod +x /usr/bin/intel-pwr-limit-adj

# Configure msr module to load on boot
echo "msr" | sudo tee /etc/modules-load.d/intel-pwr-limit-adj.conf

# Run intel-pwr-limit-adj
sudo modprobe msr
sudo intel-pwr-limit-adj

# Copy systemd service
sudo cp -v intel-pwr-limit-adj.service /etc/systemd/system/
sudo systemctl enable intel-pwr-limit-adj.service

